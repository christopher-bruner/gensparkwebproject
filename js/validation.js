const firstnameEl = document.querySelector('#firstname');
const lastnameEl = document.querySelector('#lastname');
const emailEl = document.querySelector('#email');
const phoneEl = document.querySelector('#phone');
const messageEl = document.querySelector('#message');

const form = document.querySelector('#contact-form');

const isRequired = value => value === '' ? false : true;

const isNameValid = name => {

    const regex = /^[a-zA-Z]+$/;
    return regex.test(name);

}

const isEmailValid = email => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

const isPhoneValid = phone => {

    const regex = /^\d{10}$/;
    return regex.test(phone);

}

const isValidMessage = message => {
    return (message.length > 0 && message.length <= 250);
}

const showError = (input) => {

    // const formControl = input.parentElement;

    input.classList.add('invalid');
    
    // formControl.className = 'form-control error';
    // const small = formControl.querySelector('small');
    // small.innerText = message;

}

const showSuccess = (input) => {

    // const formControl = input.parentElement;

    input.classList.remove('invalid');
    input.classList.add('valid');

    // formControl.className = 'form-control success';
    // const small = formControl.querySelector('small');
    // small.innerText = '';

}

form.addEventListener('submit', function (e) {

    e.preventDefault();

    const patterns = {
        "firstname": isNameValid,
        "lastname": isNameValid,
        "email": isEmailValid,
        "phone": isPhoneValid,
        "message": isValidMessage
    }

    const inputs = [firstnameEl, lastnameEl, emailEl, phoneEl, messageEl];

    let isValid = true;

    inputs.forEach(input => {

        const pattern = patterns[input.name];

        if (pattern) {

            if (!pattern(input.value)) {
                showError(input);
                isValid = false;
            } else {
                showSuccess(input);
            }

        }

    });

    if (isValid) {
        form.submit();
    }

});
